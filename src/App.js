import React, { Component } from 'react';
import Buscador from './components/Buscador';
import Resultado from './components/Resultado';

class App extends Component {
  state = {
    termino: '',
    images: [],
    pagina: ''
  }
  
  scroll = () => {
    const element = document.querySelector('.jumbotron');
    element.scrollIntoView('smooth','start');
  }
  paginaAnterior = () => {
    let pagina = this.state.pagina;
    if(pagina===1) return null;
    pagina -=1;
    this.setState({
      pagina 
    }, () => this.consultarApi())
    this.scroll()
  }
  paginaSiguiente = () => {
    let pagina = this.state.pagina;
    pagina +=1;
    this.setState({
      pagina 
    }, () => this.consultarApi())
    this.scroll()
  }
  consultarApi = () => {
    const termino = this.state.termino;
    const pagina = this.state.pagina;

    const url = `https://pixabay.com/api/?key=10363567-39680225fcd8aca47cd04081d&q=${termino}&per_page=30&page=${pagina}`;
    fetch(url)
      .then(res => res.json())
      .then(res => {
         this.setState({
          images: res.hits
         })
      })
  }
  datosBuesqueda = (termino) => {
    this.setState({
      termino:termino,
      pagina: 1
    },()=> {
      this.consultarApi() 
    })
  }

  render() {
    return (
      <div className="app container">
         <div className="jumbotron">
             <p className="lead text-center">Buscador de Imágenes.</p>  
             <Buscador datosBuesqueda={this.datosBuesqueda} />
         </div>
         <div className="row justify-conten-center">
               <Resultado 
                 images={this.state.images}
                 paginaAnterior={this.paginaAnterior}
                 paginaSiguiente={this.paginaSiguiente}
               />
         </div>
          
      </div>
    );
  }
}

export default App;
