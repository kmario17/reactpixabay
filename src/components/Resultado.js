import React, { Component } from 'react';
import Imagen from './ImagenItem';
import Paginacion from './Paginacion';

class Resultado extends Component {
    
    mostrarImagenes = ()=> {
       const {images} = this.props;
       if(images.length ===0) return null;
       return (
           <React.Fragment>
               <div className="col-12 p-5 row">
                  {
                      images.map(item => {
                         return (
                            <Imagen 
                               key={item.id}
                               images={item}
                            />
                         )
                      })
                  }
               </div>
               <Paginacion
                 paginaAnterior={this.props.paginaAnterior}
                 paginaSiguiente={this.props.paginaSiguiente}
               />
           </React.Fragment>
       )
    }
    render() {
        return(
            <div>
                {this.mostrarImagenes()} 
            </div>
        )
    }
}

export default Resultado;