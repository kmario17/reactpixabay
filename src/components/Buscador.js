import React, { Component } from 'react';

export default class Buscador extends Component {
    

    handleSearchMovies = (event) => {
       event.preventDefault();
       const nameMovie = document.getElementById('nameMovie').value
       this.props.datosBuesqueda(nameMovie)
    }
    render() {
       return(
           <form onSubmit={this.handleSearchMovies}>
               <div className="row">
                  <div className="form-group col-md-8">
                      <input 
                          id="nameMovie"
                          name="nameMovie"
                          type="text"
                          className="form-control form-control-lg"
                          placeholder="Busca tu Imágen. Ejemplo: Futbol"
                          
                       />
                  </div>
                  <div className="form-group col-md-4">
                      <button 
                           className="btn btn-lg btn-danger btn-block"
                       >
                           Buscar
                       </button>
                  </div>
               </div>
           </form>
       )
    }
}

